package com.example.demo;

public class Account {
  String owner_name;
  //Address address = new Address("bbsr","odisha",4134);
  private double balance_amount;
  String created_date;
  String status;
  public Account(String owner_name,String created_date,String status)
  {
      this.owner_name = owner_name;
      this.created_date = created_date;
      this.status = status;
  }
  //setter and getter method for account balance
  double getAccountBalance()
  {
      return this.balance_amount;
  }
  void setAccountBalance(double money) {
      this.balance_amount = money;
  }
  //method to deposit money in the account
  boolean deposit(double depositAmount) {
      if (depositAmount > 0.0) {
          balance_amount += depositAmount;
          return true;
      } else {
          return false;
      }
  }
  //method to withdraw money from the account
  boolean withdraw(double withdrawAmount) {
      if (withdrawAmount > this.getAccountBalance()) {
          return false;
      } else {
          this.balance_amount -= withdrawAmount;
          return true;
      }
  }
}
