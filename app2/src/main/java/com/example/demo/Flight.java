package com.example.demo;

public class Flight {
	private String flightNumber;
	private String airlineName;
	private String airlineNumber;
	String[] origin = {"BBI","HYD","BNG","CHN"};
	String[] destination = {"BBI","HYD","BNG","CHN"};
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirline(String airlineName) {
		this.airlineName = airlineName;
	}
	public String getAirlineNumber() {
		return airlineNumber;
	}
	public void setAirlineNumber(String airlineNumber) {
		this.airlineNumber = airlineNumber;
	}
}
