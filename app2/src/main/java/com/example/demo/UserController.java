package com.example.demo;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@GetMapping("/")
	void call() {
		System.out.println("Called");
	}
	@GetMapping("/{id}")
	void getUser(@PathVariable Integer id)
	{
		System.out.println("called .." +id);
	}
	@PostMapping("/id1")
	private String postcall1()
	{
		System.out.println("Post");
		return "post called";
	}
	@PostMapping("/")
	private String postcall2(@RequestBody User user)
	{
		System.out.println("got user -> "+user.name);
		return "post called";
	}
	@PutMapping
	String handlePutMapping()
	{
		return "Put called";
	}
}
