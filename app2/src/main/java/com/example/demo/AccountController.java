package com.example.demo;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account")
public class AccountController {
	//get post
	@GetMapping("/")
	public String getUserAccount()
	{
		System.out.println("called");
		return "success";
	}
	ArrayList<Account> accountList= new ArrayList<Account>();
	@PostMapping("/post")
	public String sendDetails(@RequestBody Account account) {
		accountList.add(account);
		System.out.println("data sent succesfully");
		return "sent";
	}
	@GetMapping("/retrieve")
	public ArrayList<Account> retrieve()
	{
		return accountList;
	}
	@PostMapping
	private String postcall(@RequestBody Account account)
	{
		System.out.println("got account -> "+account.owner_name);
		System.out.println("status -> "+account.status);
		System.out.println("create date -> "+account.created_date);
		return "post called";
	}
}
