package com.example.demo;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/flightMain")
public class FlightBookingContoller {
	ArrayList<Flight> flightData = new ArrayList<Flight>();
	ArrayList<UserFlight> userData = new ArrayList<UserFlight>();
	ArrayList<AdminFlight> adminData = new ArrayList<AdminFlight>();
	
	@GetMapping("/flight")
	public ArrayList<Flight> getFlight()
	{
		System.out.println("called");
		return flightData;
	}
	@GetMapping("/user")
	public ArrayList<UserFlight> getUser()
	{
		System.out.println("called");
		return userData;
	}
	@GetMapping("/admin")
	public ArrayList<AdminFlight> getAdmin()
	{
		System.out.println("called");
		return adminData;
	}
	@PostMapping("/flight")
	private String postcall(@RequestBody Flight flight)
	{
		System.out.println("Airline Name -> "+flight.getAirlineName());
		System.out.println("Flight Number -> "+flight.getFlightNumber());
		System.out.println("Flight number -> "+flight.getAirlineNumber());
		flightData.add(flight);
		return "post called";
	}
	@PostMapping("/user")
	private String postcall(@RequestBody UserFlight user)
	{
		System.out.println("First Name -> "+user.getFirstName());
		System.out.println("Middle Number -> "+user.getMiddleName());
		System.out.println("Last number -> "+user.getLastName());
		System.out.println("Data of Booking -> "+user.getDateOfBooking());
		System.out.println("Flight Departure -> "+user.getDeparture());
		System.out.println("Flight Arrival -> "+user.getArrival());
		userData.add(user);
		return "post called";
	}
	@PostMapping("/admin")
	private String postcall(@RequestBody AdminFlight admin)
	{
		System.out.println("Airline Name -> "+admin.getAdminName());
		System.out.println("Place -> "+admin.getPlace());
		System.out.println("Admin ID -> "+admin.getAdminId());
		adminData.add(admin);
		return "post called";
	}
}
