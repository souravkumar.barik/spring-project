
public class BankDriver {
  public static void main(String[] args)
  {
      //making the object as final so that It cannot be deleted
      //savings account  object 
      final Savings account1 = new Savings("sourav","01.09.2021","active");
      account1.setRate(12);
      account1.setAccountBalance(56000);
      //display details
      System.out.println("Savings Account holder name " +account1.owner_name);
      System.out.println(" created on "+account1.created_date);
      System.out.println(" account status "+account1.status);
      System.out.println(" having address "+account1.address.toString());
      System.out.println(" with rate of Interest "+account1.getRate());

      //current account object
      final Current account2 = new Current("rahul","01.09.2021", "INACTIVE");
      account2.setRate(8);
      account2.setAccountBalance(86000);
      //display details
      System.out.println("Savings Account holder name " +account2.owner_name);
      System.out.println(" created on "+account2.created_date);
      System.out.println(" account status "+account2.status);
      System.out.println(" having address "+account2.address.toString());
      System.out.println(" with rate of Interest "+account2.getRate());

  }
}
