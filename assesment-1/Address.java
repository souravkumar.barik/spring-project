public class Address {
  String city;
  String state;
  int pin;
  public Address(String city,String state, int pin)
  {
      this.city = city;
      this.state = state;
      this.pin = pin;
  }
  public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append(this.city);
      builder.append(this.state);
      builder.append(this.pin);
      return builder.toString();
  }
}
